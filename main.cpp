#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
#include <cmath>    

using json = nlohmann::json;

int main(){
    //Random generator to 898
    int id;
    srand (time(NULL));
    id = rand() % 898 + 1;
    RestClient::Response r1 = RestClient::get("https://pokeapi.co/api/v2/pokemon/" + std::to_string(id));

    std::cout << "\n";
    
    auto json_body = json::parse(r1.body);
    
    //std::cout << json_body["name"] << std::endl;
    //std::cout << json_body["types"][0]["type"]["name"] << std::endl;
    //std::cout << json_body["types"][1]["type"]["name"] << std::endl;
    //std::cout << json_body["sprites"]["front_default"] << std::endl;

    // pokemon object
    json pokemon = {
        {"name", json_body["name"]},
        {"sprites", json_body["sprites"]["front_default"]},
        {"types",{
            {"type1", json_body["types"][0]["type"]["name"]},
            {"type2", json_body["types"][1]["type"]["name"]}
        }}
    };
    pokemon.dump();
    //std::cout << pokemon.dump() << "\n";
    // new object which contains pokemon data and meta
    json pokemon_data = {
        {"data", pokemon.dump()},
        {"meta", "pokemon"}
    };
    pokemon_data.dump();
    std::cout << pokemon_data.dump() << "\n";
    // Getting all stuff from server
    RestClient::Response r = RestClient::get("http://127.0.0.1:5000/fetch/all");
    std::cout << "Response code: " << r.code << "\n";
    auto json_pokemon_data = json::parse(r.body);
    std::string string_data;
    // Looping through all data to check if there is already that pokemon which we are going to put there
    for(int i = 0; i < json_pokemon_data.size(); i++)
    {
        string_data = json_pokemon_data[i]["data"];
        auto parsed_string_data = json::parse(string_data);
        std::cout <<"Pokemon: " << parsed_string_data["name"] << std::endl;
        if(parsed_string_data["name"] == json_body["name"])
        {
            std::cout << "Pokemon already there" << std::endl;
        }

    }
    // Posting pokemon to server
    RestClient::Response r2 = RestClient::post("http://127.0.0.1:5000/submit", "application/json", pokemon_data.dump());
}