# Pokemon REST API

The idea of this project was to build a pokemon rest api request.

# Usage

You need g++11 compiler for this project.

Take a git clone on project and take it to your computer.

For starting server:

python3 main.py

For building project:

on main type $code .

In visual studio code press f1 key and type cmake and choose build

Use g++11 compiler

After building use command $./build/pokemon on terminal

# About the project

This was made by Kalle Nurminen and Ilona Skarp for Noroff and Experis Academy Finland.

# Responsibilities

Kalle and Ilona were on call all the time because Ilona's jetson nano didn't have SD card. Testing was performed with Kalle's device. The code was discussed over the call and all other stuff also.

# Maintainers

[Ilona Skarp] (https://gitlab.com/iskarp)

[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)

